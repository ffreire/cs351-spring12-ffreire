/* 
 * hello.c - the quintessential hello world program
 */
#include <stdio.h>

int main () {
  printf("Felipe Freire\n");
  printf("A20282505\n");
  printf("Why all Pascal programmers ask to live in Atlantis?\nBecause it is below C level.\n");
	return 0;
}
