#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poker.h"

/* converts a hand (of 5 cards) to a string representation, and stores it in the
 * provided buffer. The buffer is assumed to be large enough.
 */
void hand_to_string (hand_t hand, char *handstr) {
    char *p = handstr;
    int i;
    char *val, *suit;
    for (i=0; i<5; i++) {
        if (hand[i].value < 10) {
            *p++ = hand[i].value + '0';
        } else {
            switch(hand[i].value) {
				case 10: *p++ = 'T'; break;
				case 11: *p++ = 'J'; break;
				case 12: *p++ = 'Q'; break;
				case 13: *p++ = 'K'; break;
				case 14: *p++ = 'A'; break;
            }
        }
        switch(hand[i].suit) {
			case DIAMOND: *p++ = 'D'; break;
			case CLUB: *p++ = 'C'; break;
			case HEART: *p++ = 'H'; break;
			case SPADE: *p++ = 'S'; break;
        }
        if (i<=3) *p++ = ' ';
    }
    *p = '\0';
}

/* converts a string representation of a hand into 5 separate card structs. The
 * given array of cards is populated with the card values.
 */
void string_to_hand (const char *handstr, hand_t hand) {
	int i;
	int j = 0;
	
	for(i = 0; i<13; i++){
		switch(handstr[i++]) {
			case '2': hand[j].value = 2; break;
			case '3': hand[j].value = 3; break;
			case '4': hand[j].value = 4; break;
			case '5': hand[j].value = 5; break;
			case '6': hand[j].value = 6; break;
			case '7': hand[j].value = 7; break;
			case '8': hand[j].value = 8; break;
			case '9': hand[j].value = 9; break;
			case 'T': hand[j].value = 10; break;
			case 'J': hand[j].value = 11; break;
			case 'Q': hand[j].value = 12; break;
			case 'K': hand[j].value = 13; break;
			case 'A': hand[j].value = 14; break;
		}
		switch(handstr[i++]) {
			case 'D': hand[j].suit = DIAMOND; break;
			case 'C': hand[j].suit = CLUB; break;
			case 'H': hand[j].suit = HEART; break;
			case 'S': hand[j].suit = SPADE; break;
		}
		j++;
	}
}
/* sorts the hands so that the cards are in ascending order of value (two
 * lowest, ace highest */

void ace_to_one (hand_t hand){
	int i;
	for(i=0; i<5; i++){
		if(hand[i].value == 14)
			hand[i].value = 1;
	}
}

void clone_hand(hand_t hand_copy, hand_t hand){
	int i;
	for(i=0; i<5; i++){
		hand_copy[i].value = hand[i].value;
		hand_copy[i].suit = hand[i].suit;
	}
}

void sort_hand (hand_t hand) {
	int x, y;
	for(x=0; x<5; x++)
	{
		for(y=0; y<4; y++)
		{
			if(hand[y].value>hand[y+1].value)
			{
				int tempV = hand[y+1].value;
				int tempS = hand[y+1].suit;
				
				hand[y+1].value = hand[y].value;
				hand[y+1].suit = hand[y].suit;
				
				hand[y].value = tempV;
				hand[y].suit = tempS;
			}
		}
	}
}

int count_pairs (hand_t hand) {
	int i,j;
	int pairs = 0;
	int discardedV = 0;
	
	for(i=0; i<5; i++){
		int value = hand[i].value;
		for(j = i+1; j<5; j++){
			if(hand[j].value == value && value != discardedV){
				pairs++;
				discardedV = value;
			}
		}
	}
    return pairs;
}

int is_onepair (hand_t hand) {
	if(count_pairs(hand)>0)
		return 1;
	return 0;
}

int is_twopairs (hand_t hand) {
    if(count_pairs(hand)==2)
		return 1;
	return 0;
}

int is_threeofakind (hand_t hand) {
	int i,j;
	int occ = 0;
	
	for(i=0; i<5; i++){
		int value = hand[i].value;
		for(j = i+1; j<5; j++){
			if(hand[j].value == value)
				occ++;
			if(occ == 2) return 1;
		}
		occ = 0;
	}
    return 0;
}

int is_straight (hand_t hand) {
	int i;
	int fail = 0;
	card_t hand_copy[5];
	clone_hand(hand_copy, hand);
	ace_to_one(hand_copy);
	sort_hand(hand_copy);
	sort_hand(hand);
	
	for(i=0; i<4; i++){
		if(hand_copy[i+1].value - hand_copy[i].value != 1){
			fail++;
			break;
		}
	}
	
	for(i=0; i<4; i++){
		if(hand[i+1].value - hand[i].value != 1){
			fail++;
			break;
		}
	}
	if(fail==2) return 0;
	return 1;
}

int is_fullhouse (hand_t hand) {
	int i,j;
	int occ = 0;
	int discardedV = 0;
	
	for(i=0; i<5; i++){
		int value = hand[i].value;
		for(j = i+1; j<5; j++){
			if(hand[j].value == value)
				occ++;
			if(occ == 2) 
				discardedV = value;
		}
		occ = 0;
	}
	
	int pairs = 0;
	
	for(i=0; i<5; i++){
		int value = hand[i].value;
		for(j = i+1; j<5; j++){
			if(hand[j].value == value && value != discardedV)
				pairs++;
		}
	}
	
	if(pairs > 0 && discardedV != 0)
		return 1;
    return 0;
}

int is_flush (hand_t hand) {
	int i;
	for(i=0; i<4; i++){
		if(hand[i+1].suit != hand[i].suit){
			return 0;
		}
	}
    return 1;
}

int is_straightflush (hand_t hand) {
	if(is_straight(hand) && is_flush(hand))
		return 1;
    return 0;
}

int is_fourofakind (hand_t hand) {
	int i,j;
	int occ = 0;
	
	for(i=0; i<5; i++){
		int value = hand[i].value;
		for(j = i+1; j<5; j++){
			if(hand[j].value == value)
				occ++;
			if(occ == 3) return 1;
		}
		occ = 0;
	}
    return 0;
}

int is_royalflush (hand_t hand) {
	if(!is_flush(hand))
		return 0;
	
	int i;
	int total = 0;
	int suit = hand[0].suit;
	
	for(i=0; i<5; i++){ 
		if(hand[i].suit != suit) return 0;
		switch(hand[i].value) {
			case 10: total++; break;
			case 11: total++; break;
			case 12: total++; break;
			case 13: total++; break;
			case 14: total++; break;
		}
	}
	
	if(total!=5)
		return 0;
	return 1;
}

int rank_hand(hand_t hand){
	if(is_royalflush(hand))
		return 9;
	if(is_straightflush(hand))
		return 8;
	if(is_fourofakind(hand))
		return 7;
	if(is_fullhouse(hand))
		return 6;
	if(is_flush(hand))
		return 5;
	if(is_straight(hand))
		return 4;
	if(is_threeofakind(hand))
		return 3;
	if(is_twopairs(hand))
		return 2;
	if(is_onepair(hand))
		return 1;
	return 0;
}

/* compares the hands based on rank -- if the ranks (and rank values) are
 * identical, compares the hands based on their highcards.
 * returns 0 if h1 > h2, 1 if h2 > h1.
 */
int compare_hands (hand_t h1, hand_t h2) {
	int rank1 = 0;
	int rank2 = 0;
	rank1 = rank_hand(h1);
	rank2 = rank_hand(h2);
	if(rank1 > rank2) return 0;
	else if(rank2 > rank1) return 1; 
	else{
		return compare_ranks(h1, h2, rank1);
	}
}

/* compares the hands based solely on their highcard values (ignoring rank). if
 * the highcards are a draw, compare the next set of highcards, and so forth.
 */
int compare_highcards (hand_t h1, hand_t h2) {
	
	card_t hand1[5];
	clone_hand(hand1, h1);
	
	card_t hand2[5];
	clone_hand(hand2, h2);
	
	sort_hand(hand1);
	sort_hand(hand2);
	
	int i;
	for(i = 4; i>0; i--){
		if(hand1[i].value>hand2[i].value)
			return 0;
		else if(hand1[i].value<hand2[i].value)
			return 1;
	}
    return 0;
}

int max_kind(hand_t h1, hand_t h2, int maxocc){
	int i,j;
	int occ = 0;
	
	int val1, val2;
	
	for(i=0; i<5; i++){
		int value = h1[i].value;
		for(j = i+1; j<5; j++){
			if(h1[j].value == value)
				occ++;
			if(occ == maxocc){
				val1 = value;
				break;
			}
		}
		occ = 0;
	}
	occ = 0;
	for(i=0; i<5; i++){
		int value = h2[i].value;
		for(j = i+1; j<5; j++){
			if(h2[j].value == value)
				occ++;
			if(occ == maxocc){
				val2 = value;
				break;
			}
		}
		occ = 0;
	}
	
	if(val1<val2){
		return 1;
    }
	return 0;
}

int compare_ranks(hand_t h1, hand_t h2, int rank){
	switch(rank){
		case 9: return compare_highcards(h1,h2);
		case 8: return compare_highcards(h1,h2);
		case 7: return max_kind(h1,h2, 3);
		case 6: return max_kind(h1,h2, 2);
		case 5: return compare_highcards(h1,h2);
		case 4: return compare_highcards(h1,h2);
		case 3: return max_kind(h1,h2, 2);
		case 2: return max_kind(h1,h2, 1);			
		case 1: return max_kind(h1,h2, 1);				
		default: return compare_highcards(h1,h2);
	}
}

