#include <stdlib.h>
#include <string.h>
#include "graph.h"

//External and global variables
//Array containing the names of all the visited vertexes
char *visited[1000];
//Mantains the current size of the array for further comparison
int size = 0;

void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight) {
	vertex_t *v1, *v2, *vp, *vp2, *vaux, *vfinal;
	adj_vertex_t *adj_v;
	
	vp = findVertex(v1_name, vtxhead);
	vp2 = findVertex(v2_name, vtxhead);
	
	//Finds the last vertex of the list, so that the new one can come after it
	for (vaux = *vtxhead; vaux != NULL; vaux = vaux->next){
		vfinal = vaux;
	}
	
	//If none of the vertexes already exists, creates them both
	if(vp==NULL && vp2==NULL){
		v1 = malloc(sizeof(vertex_t));
		v1->name = v1_name;
		
		v2 = malloc(sizeof(vertex_t));
		v2->name = v2_name;
		
		v1->next = v2;
		v2->next = NULL;
		
		adj_v = v1->adj_list = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = v2;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
		adj_v = v2->adj_list = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = v1;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
		if(*vtxhead == NULL){
			*vtxhead = v1;
		}
		else{
			vfinal->next = v1;
		}
	}
	//If v1 already exists
	else if(vp!=NULL && vp2==NULL){
		v2 = malloc(sizeof(vertex_t));
		v2->name = v2_name;
		v2->next = NULL;
		vfinal->next = v2;
		
		
		adj_v = lastVertex(vp);
		
		adj_v = adj_v->next = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = v2;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
		
		adj_v = v2->adj_list = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = vp;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
	}
	//If v2 already exists
	else if(vp==NULL && vp2!=NULL){
		v1 = malloc(sizeof(vertex_t));
		v1->name = v1_name;
		v1->next = NULL;
		vfinal->next = v1;
		
		adj_v = v1->adj_list = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = vp2;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
		
		adj_v = lastVertex(vp2);
		
		adj_v = adj_v->next = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = v1;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
	}
	//If both the vertexes already exist
	else{
		adj_v = lastVertex(vp);
		
		adj_v = adj_v->next = malloc(sizeof(adj_vertex_t));
		adj_v->vertex = vp2;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
		
		
		adj_v = lastVertex(vp2);
		
		adj_v = adj_v->next = malloc(sizeof(adj_vertex_t));		
		adj_v->vertex = vp;
		adj_v->edge_weight = weight;
		adj_v->next = NULL;
	}
	
}

void tour(vertex_t **vtxhead, int*total){
	adj_vertex_t *adj_v;	
	vertex_t *vp;
	visited[size] = (*vtxhead)->name;
	size++;
	for (adj_v = (*vtxhead)->adj_list; adj_v != NULL; adj_v = adj_v->next){
		if(!contains(adj_v->vertex->name)){
			vp = findVertex(adj_v->vertex->name, vtxhead);
			if(vp!=NULL){
				*total += adj_v->edge_weight;
				tour(&vp,total);
			}
		}
	}
}

int contains(char *search){
	int i;
	for(i = 0; i<size; i++){
		if(strcmp(visited[i],search)==0) return 1;
	}
	return 0;
}

vertex_t *findVertex(char *name, vertex_t **vtxhead){
	vertex_t *vp;
	for (vp = *vtxhead; vp != NULL; vp = vp->next) {
		if(strcmp(vp->name,name)==0){
			return vp;
		}
	}
	return NULL;
}

adj_vertex_t *lastVertex(vertex_t *vp){
	adj_vertex_t *adj_v, *adj_vaux;
	for (adj_vaux = vp->adj_list; adj_vaux != NULL; adj_vaux = adj_vaux->next){
		adj_v = adj_vaux;
	}
	return adj_v;
}

void freeAll(vertex_t **vtxhead){
	vertex_t *vp;
	adj_vertex_t *adj_v;
	
	for (vp = *vtxhead; vp != NULL; vp = vp->next) {
		for (adj_v = vp->adj_list; adj_v != NULL; adj_v = adj_v->next) {
			free(adj_v);
		}
	}
	
	for (vp = *vtxhead; vp != NULL; vp = vp->next) {
		free(vp);
	}	
}