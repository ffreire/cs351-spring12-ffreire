#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int main (int argc, char *argv[]) {
	int it;
	vertex_t *vlist_head = malloc(sizeof(vertex_t));
	vlist_head = NULL;
	
	char *v1, *v2;
	int weight;
	for(it = 1; it<argc; it++){
		v1 = argv[it++];
		v2 = argv[it++];
		weight = atoi(argv[it]);
		add_edge(&vlist_head,v1,v2,weight);
	}
	
	vertex_t *vp;
	adj_vertex_t *adj_v;
	
	//Counts how many vertexes were created
	int count = 0;
	printf("Adjacency list:\n");
	for (vp = vlist_head; vp != NULL; vp = vp->next) {
		count++;
		printf("  %s: ", vp->name);
		for (adj_v = vp->adj_list; adj_v != NULL; adj_v = adj_v->next) {
			printf("%s(%d) ", adj_v->vertex->name, adj_v->edge_weight);
		}
		printf("\n");
	}
	
	printf("\n");
	int total = 0;

	tour(&vlist_head, &total);
	printf("Tour path: \n");
	
	//If not all the vertexes were reached, then there's no tour
	if(size<count) printf("No tour exists\n");
	//Else, prints the tour and it's length
	else{
		int i;
		for(i = 0; i<size; i++){
			printf("%s ",visited[i]);
		}
		printf("\n\n");
	
	printf("Tour length: %d\n", total);
		
	}
	
	freeAll(&vlist_head);
	return 0;
}
